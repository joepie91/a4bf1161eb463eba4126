Keeping a list here of stuff that I run into while researching OpenTTD's NML format. Specifically, stuff needed to parse/convert it.

# Format documentation

* [Wiki](http://newgrf-specs.tt-wiki.net/wiki/NML:Language_files)
* [Single page](http://devs.openttd.org/~planetmaker/patches/nml-language.html)

# Parsing

* [Canopy parser](http://canopy.jcoglan.com/)
* [Example PEG grammar](https://github.com/jcoglan/canopy/blob/master/examples/canopy/peg.peg)
* [Creating own language made easy (slides)](http://www.slideshare.net/RReverser/creating-own-language-made-easy)

# GRF stuff

* [TTDPatch documentation on NFO/GRF](http://www.ttdpatch.de/grfspecs/)